package click.shadyurl.api

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "generator")
data class GeneratorConfiguration(
    val appDomain: String,
    val baseDomain: String,
    val pickedWordsCount: Int,
    val randomHexadecimal: Boolean = false
)