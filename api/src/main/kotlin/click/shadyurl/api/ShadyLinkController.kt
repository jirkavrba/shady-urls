package click.shadyurl.api

import jakarta.validation.Valid
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMethod

@Controller
class ShadyLinkController(
    private val service: ShadyLinkService,
    private val configuration: GeneratorConfiguration,
) {

    @GetMapping("/")
    suspend fun index(): ResponseEntity<Unit> {
        return ResponseEntity.status(HttpStatus.TEMPORARY_REDIRECT)
            .header(HttpHeaders.LOCATION, "https://${configuration.appDomain}")
            .build()
    }

    @GetMapping("/{path:.*}")
    suspend fun redirect(@PathVariable path: String): ResponseEntity<Unit> {
        return service.resolveUrl(path)
            ?.let {
                ResponseEntity.status(HttpStatus.TEMPORARY_REDIRECT)
                    .header(HttpHeaders.LOCATION, it)
                    .header(HttpHeaders.CONNECTION, "close")
                    .build()
            }
            ?: index()
    }

    @CrossOrigin(
        origins = ["*"],
        methods = [RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS],
    )
    @PostMapping("/api/redirect/create")
    suspend fun create(@Valid @RequestBody request: CreateShadyLinkRequest): ResponseEntity<CreateShadyLinkResponse> {
        val link = service.generateLink(request.url, request.permanent)
        val response = CreateShadyLinkResponse(
            link.url,
            request.url,
            request.permanent
        )

        return ResponseEntity.ok(response)
    }

}