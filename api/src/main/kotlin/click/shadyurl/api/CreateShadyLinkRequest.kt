package click.shadyurl.api

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern

data class CreateShadyLinkRequest(
    @field:NotBlank
    @field:Pattern(regexp = "^https?://.*", message = "The provided URL must start with either http:// or https://")
    val url: String,
    val permanent: Boolean = false
)