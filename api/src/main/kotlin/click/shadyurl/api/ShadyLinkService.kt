package click.shadyurl.api

import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.stereotype.Service
import java.time.Duration

@Service
class ShadyLinkService(
    private val redis: ReactiveRedisOperations<String, String>, private val generator: ShadyLinkGenerator
) {
    suspend fun generateLink(url: String, permanent: Boolean = false): ShadyLink {
        val link = generator.generateLink(url)

        val ops = redis.opsForValue()
        val action = if (permanent) {
            ops.set(link.path, url)
        } else {
            ops.set(link.path, url, Duration.ofHours(24))
        }

        action.awaitSingle()

        return link
    }

    suspend fun resolveUrl(path: String): String? {
        return redis.opsForValue().get(path).awaitSingleOrNull()
    }
}