package click.shadyurl.api

data class CreateShadyLinkResponse(
    val link: String,
    val redirect: String,
    val permanent: Boolean
)