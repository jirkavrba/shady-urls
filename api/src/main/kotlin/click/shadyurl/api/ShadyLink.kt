package click.shadyurl.api

data class ShadyLink(
    val path: String,
    val url: String
)