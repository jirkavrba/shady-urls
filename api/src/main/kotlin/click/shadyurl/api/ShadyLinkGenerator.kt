package click.shadyurl.api

import org.springframework.stereotype.Service
import kotlin.random.Random

@Service
class ShadyLinkGenerator(
    private val configuration: GeneratorConfiguration
) {

    private val subdomains = setOf(
        "facebook.com", "google.com", "gmail.com",
        "instagram.com", "amazon.com", "github.com",
        "youtube.com", "reddit.com", "netflix.com",
        "office.com", "microsoft.com", "spotify.com",
        "pornhub.com", "onlyfans.com", "twitter.com",
    )

    private val words = setOf(
        "facebook.com", "amazon.com", "paypal.com", "Win a free iPad", "ISIS registration form", "Bush did 9 11",
        "trojan", "Trojan.Win32", "Genocide", "KKK", "Ku Klux Klan", "Heroin", "Cocaine", "Meth", "Weed", "Download",
        "Free", "Hentai", "Porn", "weeb", "Twitter Hack", "Facebook Hack", "Crypto", "Bitcoin", "Stolen credit cards",
        "Phishing", "White Power", "Tentacle fun time", "Windows Crack", "Free Money", "Webhost000", "Invoice",
        "Java Exploit", "Warez", "Cracked", "Botnet", "Dark Net", "xxx", "Adolf will rise", "IP Stealer",
        "Token grabber", "Discord hack", "Leaked nudes", "OnlyFans", "License key", "Secret conspiracy",
        "Illegal substances", "Midget porn", "Furry", "420", "Hot moms nearby", "Hot single women",
        "Penis enlargement pills", "Tinder", "Click Here", "FBI", "CIA", "NSA", "Donald Trump", "Android hack", "Malware",
        "Keylogger", "Root", "Win $100 000 000", "Hidden service", "Tor", "Onion", "Terrorism", "Yiff",
        "Tesla", "e621.net", "nhentai.net", "Trial version", "Try not to cum", "Challenge", "CSGO cheats",
        "Minecraft", "Minecraft cheat", "Free Minecraft", "Nitro", "Free Nitro", "Steam gift cards",
        "Google Play gift cards", "Netflix credentials", "Roblox", "Robux", "Free Robux", "Hacked Roblox Client",
        "Hacked Minecraft Client", "Send Nudes"
    )

    private val extensions = setOf(
        "pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx",
        "txt", "bin", "png", "jpg", "gif", "webp", "mp4", "mp3", "ics"
    )

    private val finalExtensions = setOf(
        "zip", "rar", "exe", "msi", "jar", "bat", "7z", "ps1"
    )

    suspend fun generateLink(url: String): ShadyLink {
        val words = words.shuffled().take(configuration.pickedWordsCount).joinToString("-") { it.normalize() + divider() }
        val subdomain = subdomains.shuffled().first()
        val extension = extensions.shuffled().first()
        val finalExtension = finalExtensions.shuffled().first()
        val path = "${words}.${extension}.${finalExtension}"

        val link = "https://${subdomain}.${configuration.baseDomain}/${path}"

        return ShadyLink(path, link)
    }

    private fun divider(): String {
        if (!configuration.randomHexadecimal) {
            return ""
        }

        val pool = ('0'..'9') + ('a'..'f')
        val characters = List(6) { pool.random() }.joinToString("")

        return "-${characters}"
    }

    private fun String.normalize(): String {
        return lowercase().replace("\\s+".toRegex(), "_")
    }
}