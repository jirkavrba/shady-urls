export type Redirect = {
    link: string;
    redirect: string;
    permanent: boolean;
};

export const createRedirect = async (url: string, permanent: boolean = false): Promise<Redirect> => {
    const response = await fetch("https://api.shadyurl.click/api/redirect/create", {
        method: "post",
        credentials: "omit",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            url,
            permanent
        })
    });

    return response.json()
};
