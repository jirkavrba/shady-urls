import { useLocalStorage } from "@uidotdev/usehooks";
import {Redirect} from "./api.ts";
import {CreateRedirectModal} from "./components/CreateRedirectModal.tsx";
import ShadyRedirect from "./components/ShadyRedirect.tsx";

function App() {
    const [redirects, setRedirects] = useLocalStorage<Array<Redirect>>("redirects", []);

    const onRedirectCreate = (redirect: Redirect) => {
        setRedirects(current => [redirect, ...current]);
    };

    return (
        <div className="container text-center mx-auto my-8">
            <h1 className="text-white from-cyan-500 font-black text-3xl lg:text-6xl py-8">Shady URLs</h1>
            <h2 className="text-primary font-bold mb-5 mx-4">Instead of shortening links, why don't you make them look sketchy af?</h2>

            <CreateRedirectModal onCreate={(redirect) => onRedirectCreate(redirect)}/>

            <div className="mb-10">
                <a href="https://gitlab.com/jirkavrba/shady-urls" target="_blank" className="text-neutral-700 text-xs hover:text-primary hover:underline">Source code available on GitLab</a>
            </div>

            {redirects.map((redirect, i) =>
                <ShadyRedirect {...redirect} key={i}/>
            )}
        </div>
    )
}

export default App
