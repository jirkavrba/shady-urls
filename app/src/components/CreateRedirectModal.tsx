import {FC, useState} from "react";
import {createRedirect, Redirect} from "../api.ts";
import {
    Button,
    Input,
    Modal,
    ModalBody,
    ModalContent,
    ModalFooter,
    ModalHeader,
    Switch,
    useDisclosure
} from "@nextui-org/react";

export type CreateRedirectFormProps = {
    onCreate: (redirect: Redirect) => void;
};

export const CreateRedirectModal: FC<CreateRedirectFormProps> = ({onCreate}) => {
    const {isOpen, onOpen, onClose} = useDisclosure();

    const [url, setUrl] = useState<string>("");
    const [permanent, setPermanent] = useState<boolean>(false);

    const [loading, setLoading] = useState<boolean>(false);

    const create = async () => {
        setLoading(true);
        onCreate(await createRedirect(url, permanent));

        setUrl("");
        setPermanent(false);
        setLoading(false);
    };

    const disabled = !url.startsWith("http://") && !url.startsWith("https://");

    return (
        <>
            <div className="flex items-center justify-center p-8 lg:p-16">
                <Button color="primary" size="lg" onClick={() => onOpen()}>Create a new shady URL</Button>
            </div>
            <Modal isOpen={isOpen} hideCloseButton={true}>
                <ModalContent>
                    <>
                        <ModalHeader>
                            Create a new shady URL
                        </ModalHeader>
                        <ModalBody>
                            <Input type="url" label="Target url" value={url} onValueChange={(url) => setUrl(url)}/>
                            <Switch onValueChange={(selected) => setPermanent(selected)} defaultSelected={permanent} className="mt-3 mx-auto">
                                Keep this link permanently active
                            </Switch>
                        </ModalBody>
                        <ModalFooter className="flex flex-col items-center justify-center">
                            <Button color="primary" size="lg" tabIndex={0} onPress={() => create().then(() => onClose())} isLoading={loading} isDisabled={disabled}>
                                Create redirect
                            </Button>
                            <Button color="danger" variant="light" onPress={onClose} tabIndex={1} isDisabled={loading}>
                                Nevermind
                            </Button>
                        </ModalFooter>
                    </>
                </ModalContent>
            </Modal>
        </>
    )

};