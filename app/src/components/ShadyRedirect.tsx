import {FC} from "react";
import {Card, CardBody, CardFooter, Link, CardHeader} from "@nextui-org/react";
import {ArrowDown, ArrowRight} from "react-feather";

type ShadyRedirectProps = {
    link: string;
    redirect: string;
    permanent: boolean;
};

const ShadyRedirect: FC<ShadyRedirectProps> = ({link, redirect, permanent}) => {
    return (
        <Card className="mb-5 mx-5">
            <CardHeader/>
            <CardBody className="flex flex-col xl:flex-row justify-center items-center">
                <Link href={link} isExternal>
                <code className="text-xs">{link}</code>
                </Link>
                <ArrowDown className="w-5 h-5 my-5 block xl:hidden"/>
                <ArrowRight className="w-5 h-5 m-5 hidden xl:block"/>
                <code className="text-xs">{redirect}</code>
            </CardBody>
            <CardFooter className="justify-center text-default-200 text-xs">
                {permanent ? "Never expires" : "Expires in 24 hours"}
            </CardFooter>
        </Card>
    );

};

export default ShadyRedirect;